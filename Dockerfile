FROM centos:7

RUN yum update -y && \
    yum --enablerepo=extras install -y epel-release && \
    yum groupinstall -y "Development tools" && \
    yum install -y \
	http://resources.ovirt.org/pub/yum-repo/ovirt-release40.rpm \
	createrepo \
	libxslt
